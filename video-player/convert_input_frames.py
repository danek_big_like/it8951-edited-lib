import os
import json
import time

from time import sleep
import argparse

from integration.functions_without_display import *

#
# from IT8951.display import AutoEPDDisplay
#
# print('Initializing EPD...')
#
## here, spi_hz controls the rate of data transfer to the device, so a higher
## value means faster display refreshes. the documentation for the IT8951 device
## says the max is 24 MHz (24000000), but my device seems to still work as high as
## 80 MHz (80000000)
# display = AutoEPDDisplay(vcom=-2.15, rotate=None, mirror=False, spi_hz=24000000)
#
# print('VCOM set to', display.epd.get_vcom())


ConfigFile = open('config.json')
config = json.load(ConfigFile)
print(config)

for root, dirs, files in os.walk(config["input_frames_folder"]):
    for filename in sorted(files):
        print(filename)
        start = time.perf_counter()
        convert(config["input_frames_folder"] + "/" + filename, config["output_frames_folder"], config["debug_level"],
                config)
        end = time.perf_counter()
