

# functions defined in this file
# __all__ = [
#     'print_system_info',
#     'clear_display',
#     'display_gradient',
#     'display_image_8bpp',
#     'partial_update'
# ]

from PIL import Image, ImageDraw, ImageFont, ImagePalette

from sys import path

path += ['../../']


def convert(img_path, new_path, debug, config):
    img = Image.open(img_path)

    new_img_paths = img_path.replace(config["input_frames_folder"], new_path)

    img2 = img.convert('1')
    img2.save(new_img_paths, "PNG")
