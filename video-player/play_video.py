import math
import os
import json
import time

from time import sleep
import argparse

from integration.functions import *

from IT8951.display import AutoEPDDisplay

print('Initializing EPD...')

# here, spi_hz controls the rate of data transfer to the device, so a higher
# value means faster display refreshes. the documentation for the IT8951 device
# says the max is 24 MHz (24000000), but my device seems to still work as high as
# 80 MHz (80000000)
display = AutoEPDDisplay(vcom=-2.15, rotate=None, mirror=False, spi_hz=24000000)

print('VCOM set to', display.epd.get_vcom())


ConfigFile = open('config.json')
config = json.load(ConfigFile)
if config["debug_level"] == "high":
    print(config)


if config["screen_framespersec"] == "dyn":
    first_frame = ""
    for root, dirs, files in os.walk(config["folder"]):
        sorted(files)
        first_frame = files[0]
    start = time.perf_counter()
    if config["mode"] == "2c":
        display_image_2c_dithering(display, config["output_frames_folder"] + "/" + first_frame, config["render_frames_before_displaying"], config["debug_level"])
    elif config["mode"] == "8bpp":
        display_image_8bpp(display, config["output_frames_folder"] + "/" + first_frame, config["render_frames_before_displaying"], config["debug_level"])
    end = time.perf_counter()
    if config["debug_level"] == ("medium" or "high"):
        print(f"Time taken is {end - start}")
    displaySpeed_framespersec = 1 / (round(end - start, 1))
    skipframes = config["original_framespersec"] / displaySpeed_framespersec
    currentframe = 1
    for root, dirs, files in os.walk(config["output_frames_folder"]):
        for filename in sorted(files):
            if currentframe % skipframes < 1:
                if config["debug_level"] == "high":
                    print(filename)
                start = time.perf_counter()
                if config["mode"] == "2c":
                    display_image_2c_dithering(display, config["output_frames_folder"] + "/" + first_frame, config["render_frames_before_displaying"], config["debug_level"])
                elif config["mode"] == "8bpp":
                    display_image_8bpp(display, config["output_frames_folder"] + "/" + first_frame, config["render_frames_before_displaying"], config["debug_level"])
                end = time.perf_counter()
                if config["debug_level"] == ("medium" or "high"):
                    print(f"Time taken is {end - start}")
            currentframe += 1
else:
    displaySpeed_framespersec = int(config["screen_framespersec"])
    skipframes = config["original_framespersec"] / displaySpeed_framespersec
    currentframe = 0
    for root, dirs, files in os.walk(config["output_frames_folder"]):
        for filename in sorted(files):
            if currentframe % skipframes < 1:
                if config["debug_level"] == "high":
                    print(filename)
                start = time.perf_counter()
                display_image_2c_dithering(display, config["output_frames_folder"] + "/" + filename, True, config["debug_level"])
                end = time.perf_counter()
                if config["debug_level"] == ("medium" or "high"):
                    print(f"Time taken is {end - start}")
            currentframe += 1
