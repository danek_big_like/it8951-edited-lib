import os
import json
import time

from time import sleep
import argparse

from integration.test_functions import *

from IT8951.display import AutoEPDDisplay

print('Initializing EPD...')

# here, spi_hz controls the rate of data transfer to the device, so a higher
# value means faster display refreshes. the documentation for the IT8951 device
# says the max is 24 MHz (24000000), but my device seems to still work as high as
# 80 MHz (80000000)
display = AutoEPDDisplay(vcom=-2.15, rotate=None, mirror=False, spi_hz=24000000)

print('VCOM set to', display.epd.get_vcom())


ConfigFile = open('config.json')
config = json.load(ConfigFile)
print(config)



displaySpeed_framespersec = 1.2
skipframes = config["originalframespersec"] / displaySpeed_framespersec
currentframe = 0
for root, dirs, files in os.walk(config["folder"]):
    for filename in sorted(files):
        if currentframe % skipframes < 1:
            print(filename)
            start = time.perf_counter()
            display_image_2bpp_dithering(display, config["folder"]+"/"+filename, True, "high")
            end = time.perf_counter()
            print(f"Time taken is {end - start}")
        currentframe += 1
